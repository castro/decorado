export class User {
  id: number;
  login: string;
  password: string;
  profile: string;
}
